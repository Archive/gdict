/* 
 * Michael Bacarella wrote the original incarnation of this program.
 * All I did was add a few loops to handle the data better.
 * Thanks goes out to #linuxos also.  :)
 */

#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>

int main (int argc, char *argv[]) {

	struct sockaddr_in	Sockaddr;
	int	err;
	int 	sd;
	char	buffer[512];
	char    buffer_bin[65535];
	char    temp[12];
	char	eof = '\200';
	char	*location;
	int     nchars;

	if (argc < 2) {
		printf("Usage: %s <word to define>\n", argv[0]);
		exit(1);
	}

	sd = socket (AF_INET, SOCK_STREAM, 0);
	if (sd == -1) {
		fprintf(stderr, "Socket error!\n");
		exit(1);
	}

	sprintf (buffer, "DEFINE %s\n", argv[1]);
	
	Sockaddr.sin_family		= AF_INET;
	Sockaddr.sin_port		= htons(2627);
	Sockaddr.sin_addr.s_addr	= inet_addr("128.52.39.7");

	err = connect (sd, &Sockaddr, sizeof(Sockaddr) );
	
	if (err != 0) {
		printf("Could not connect to dictionary server!\n");
		exit(1);
	}

	err = send (sd, buffer, strlen(buffer), 0);
	
	if ((err = 0)) {
		printf("Send error!\n");
	}

	strcpy (buffer_bin, "");  /* Clear the string */
        
	memset (buffer, 0, sizeof (buffer));
	sprintf(temp, "SPELLING 0");  

        while (1) {
		if ((nchars = recv(sd, buffer, sizeof(buffer), 0) ) >= 1) {
			strncat (buffer_bin, buffer, nchars);
		}
                
		if ( strchr(buffer, eof) ) {
			location = strchr(buffer_bin, eof);
			*location = '\0';
			break;
		}
        
		if ( strstr(buffer, temp) ) {
			break;
		}
	}

	printf("%s\n", buffer_bin);
	
	exit(0);
}

